/**
* Name: Yarelit Mendoza
* Lab/task: Lab 8
* Date: 10/21/20
**/

#include "bank.h"

int numOfCustomers;        // the number of customers of the bank
int numOfAccounts;         // the number of accounts offered by the bank

double *available;    // the amount available of each customer
double **maximum;     // the maximum demand of each customer
double **allocation;  // the amount currently allocated to each customer
double **need;     // the remaining needs of each customer

/***
 * Read the state of the bank from a file.
 *
 * The file format is as follows:
 *
 * <num of customers> <num of accounts>
 * <initial state of accounts>
 * <max needs for each customer>
 *
 * e.g.,
 *
 * 5 3
 * 15 10 5
 * 7,5,3
 * 3,2,2
 * 9,0,2
 * 2,2,2
 * 4,3,3
 *
 * Any of  ", \n\t" is a valid separator
 *
 */
void loadBankState()
{
    // create the bank
    initBank();

    // read initial values for maximum array
    for (int i = 0; i < numOfCustomers; i++)
        addBankCustomer(i);
}

/***
 * Initialize the bank for a number of accounts and customers.
 */
void initBank()
{
    double in[2];
    readLine(in);
    numOfCustomers = (int) in[0];
    numOfAccounts = (int) in[1];

    available = calloc(numOfAccounts, sizeof(double));
    readLine(available);

    // initialize the accounts
    maximum = calloc(numOfCustomers, sizeof(double *));
    allocation = calloc(numOfCustomers, sizeof(double *));
    need = calloc(numOfCustomers, sizeof(double *));
}

/***
 * This function adds a customer to the bank system.
 * It records its maximum fund demand with the bank.
 */
void addBankCustomer(int customerNum)
{
    maximum[customerNum] = calloc(numOfAccounts, sizeof(double));
    readLine(maximum[customerNum]);

    allocation[customerNum] = calloc(numOfAccounts, sizeof(double));
    // we start with zero allocated

    need[customerNum] = calloc(numOfAccounts, sizeof(double));
    arraycpy(need[customerNum], maximum[customerNum], numOfAccounts);
}

/***
 * Outputs the state of the bank; i.e., funds in each account of each customer
 */
void displayBankState()
{
    printf("%13s", "Available =");
    display(available, numOfAccounts);

    printf("\n%13s", "Allocation =");
    for (int i = 0; i < numOfCustomers; i++)
        display(allocation[i], numOfAccounts);

    printf("\n%13s", "Max =");
    for (int i = 0; i < numOfCustomers; i++)
        display(maximum[i], numOfAccounts);

    printf("\n%13s", "Need =");
    for (int i = 0; i < numOfCustomers; i++)
        display(need[i], numOfAccounts);

    printf("\n");
}

/***
 * Determines whether the currect system state is safe.
 */
bool isSafeState(int customerNum, double *request)
{
    // TODO implement
    bool flag = true;
    /* step 1 */
    /* let work and finish be vectors of length m and n respectively.*/
    // work = available
    double *work = calloc(numOfCustomers, sizeof(double *));
    arraycpy(work, available, numOfCustomers);
    //finish[i] = false for i = 0, 1, n-1
    bool finish[numOfCustomers];
    set(false, finish, numOfCustomers);

    /* step 2 */
    // find an i such that both finish[i] = false and need[i]   <= work
    bool cont = true;
    for (int i = 0; i < numOfCustomers && cont; i++) {
        if ((lessOrSame(need[i], work, numOfCustomers)) && (finish[i] == false)) {
            /* step 3 */
            // work = work + allocation[i]
            add(work, allocation[customerNum], numOfCustomers);
            // finish[i] = true
            finish[customerNum] = true;
            // go to step 2: cont is still true so it will go through loop until false
        }
        else{
            /* step 4 */
            flag = allTrue(finish, numOfCustomers);
            //if safe state -> the requested resources can be allocated to process Pi
            //if state unsafe -> Pi must wait, and old resource-allocation state is restored from the saved values
            if(flag == false){
                add(available, request, numOfCustomers);
                sub(allocation[customerNum], request, numOfCustomers);
                add(need[customerNum], request, numOfCustomers);
            }
            cont = false;
        }
    }
    return flag; // TODO: modify as appropriate
}
/***
 * Determines whether a request for loan can be granted.
 */
bool solvencyTest(int customerNum, double *request)
{
    // TODO implement
    //printf("\nIn solvency test %f, %f, %f, %f, %f, %f", available[0], request[0], available[1], request[1], available[2], request[2]);
    bool flag = false;
    /* step 1: if request[i] <= need[i] go to step 2 */
    if(lessOrSame(request, need[customerNum], numOfCustomers) == true){
        /* step 2: if request[i] <= available go to step 3 */
        if((lessOrSame(request, available, numOfCustomers)) == true){
            /* step 3 */
            // pretend to allocate requested resources to Pi by modifying the state as follows
            // available = available - request[i]
            sub(available, request, numOfCustomers);
            // allocation[i] = allocation[i] + request[i]
            add(allocation[customerNum], request, numOfCustomers);
            //need[i] = need[i] - request[i]
            sub(need[customerNum], request, numOfCustomers);

            /*now run the safety algorithm*/
            flag = isSafeState(customerNum, request);

            if(flag){
                add(available, request, numOfCustomers);
                // allocation[i] = allocation[i] + request[i]
                sub(allocation[customerNum], request, numOfCustomers);
                //need[i] = need[i] - request[i]
                add(need[customerNum], request, numOfCustomers);
            }
        }
    } else{
        printf("\nCustomer cannot ask for more assets than the allowed maximum.");
    }
    return flag; // TODO: modify as appropriate
}

/***
 * Make a request for a loan.
 */

bool borrow(int customerNum, double funds[])
{
    bool ret = solvencyTest(customerNum, funds);

    // TODO: complete
    if(ret == true){
        add(allocation[customerNum], funds, numOfCustomers);
        sub(need[customerNum], funds, numOfCustomers);
        sub(available, funds, numOfCustomers);
    }

    return ret;
}

/***
 * Repay a loan.
 */
bool repay(int customerNum, double funds[])
{
    bool ret = lessOrSame(funds, allocation[customerNum], numOfAccounts);

    // TODO: complete
    if(ret == true){
        sub(allocation[customerNum], funds, numOfCustomers);
        add(need[customerNum], funds, numOfCustomers);
        add(available, funds, numOfCustomers);
    } else{
        printf("\nCustomer cannot return funds that are not allocated currently");
    }
    return ret;
}


/***
 * In a loop, take input from the standard console until 'Q' or 'q' is entered.
 *
 * Each input should have the following syntax:
 *;
 * {Q[UIT] | ST[ATUS] | [BO[RROW] | RE[PAY]] <customer number>  {<account funds> ...}}
 *
 * e.g. (for 3 accounts and with at least 2 customers):
 *
 * ST
 * BO 1 3 2 1
 * RE 1 1 0 1
 * QUIT
 *
 */
void bankTeller()
{
    // now loop reading requests to withdraw or deposit funds
    double request[numOfAccounts];
    char *inp = malloc(MAX_LINE_SIZE * sizeof(char));
    while (true)
    {
        printf("Teller: How may I help you? > ");

        if (fgets(inp, MAX_LINE_SIZE, stdin) == 0)
            break;

        if (strlen(inp) == 0)
            goto exception;

        // need a copy, since "line" will be used for tokenizing, so
        // it will change until it gets NULL; we would nnot be able to re-use it
        char *line = strdup(inp);

        // get transaction type - borrow (BO) or repay (RE) or ST (status)
        line[strlen(line) - 1] = '\0';

        if (line == NULL)
            goto exception;

        char *trans = strsep(&line, " ,\t");
        strToUpper(&trans); // so the case does not matter

        printf("REQUEST: %s\n", trans);
        if (trans[0] == 'Q')
        {
            displayBankState();
            exit(1);
        }
        else if (strncmp(trans, "STATUS", 2) == 0)
            displayBankState();
        else
        {
            if ((strncmp(trans, "BORROW", 2) != 0) && (strncmp(trans, "REPAY", 2) != 0))
                goto exception;

            // get the customer number making the tranaction
            if (line == NULL)
                goto exception;

            int custNum = strtod(strsep(&line, " "), NULL);
            printf("CLIENT %d: ", custNum);

            if (custNum >= numOfCustomers)
                goto exception;

            // get the resources involved in the transaction
            for (int i = 0; i < numOfAccounts; i++)
            {
                if (line == NULL)
                    goto exception;

                request[i] = strtod(strsep(&line, " ,\t"), NULL);
            }

            display(request, numOfAccounts);

            // now check the transaction type
            if (strncmp(trans, "BORROW", 2) == 0)
            {
                if (borrow(custNum, request))
                    printf("\n*APPROVED*\n");
                else
                    printf("\n*DENIED*\n");
            }
            else if (strncmp(trans, "REPAY", 2) == 0)
            {
                if (repay(custNum, request))
                    printf("\n*APPROVED*\n");
                else
                    printf("\n*DENIED*\n");
            }
            continue;
            exception:
            printf("\nExpected input: Q[UIT] | ST[ATUS] | [BO[RROW] | RE[PAY]] <customer number> <resource #1> <#2> <#3> ...\n");
            continue;
        }
    }
}

/***
 * Read a line of values separated by a set of delimeters from a file into an array
 */
void readLine(double array[])
{
    char *line = malloc(MAX_LINE_SIZE * sizeof(char));
    fgets(line, MAX_LINE_SIZE, stdin);
    char *tok;
    int i = 0;
    while ((tok = strsep(&line, ", \n\t")) != NULL)
        if (strlen(tok) > 0)
            array[i++] = strtod(tok, NULL);

    free(line);
}

/***
 * Convert the string to upper case
 */
void strToUpper(char **s)
{
    for (char *c = *s; *c; c++)
        if (isascii(*c) && islower(*c))
            *c = toupper(*c);
}
